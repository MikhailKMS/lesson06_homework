package ru.sber.jd.dto;

import lombok.Getter;

@Getter
public class MyBD {

    private String nameBD;
    private String conntctionString;

    public MyBD(String nameBD, String conntctionString) {
        this.nameBD = nameBD;
        this.conntctionString = conntctionString;
    }

}
