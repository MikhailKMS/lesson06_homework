package ru.sber.jd.dto;

import ru.sber.jd.exceptions.ConnectionNotSetException;
import ru.sber.jd.exceptions.RequestIsEmptyException;

public interface DataBase {

    String readFromBD(MyBD myBD, String selectRequest) throws ConnectionNotSetException, RequestIsEmptyException;

}
