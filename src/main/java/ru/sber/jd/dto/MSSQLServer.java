package ru.sber.jd.dto;

import ru.sber.jd.exceptions.ConnectionNotSetException;
import ru.sber.jd.exceptions.RequestIsEmptyException;

public class MSSQLServer implements DataBase {

    private Boolean openBD(MyBD myBD) {

        if (!myBD.getNameBD().equals("") && !myBD.getConntctionString().equals("")) {
            return true;
        } else {
            return false;
        }

    }


    public String readFromBD(MyBD myBD, String selectRequest) throws ConnectionNotSetException, RequestIsEmptyException {
        if (!openBD(myBD)) {
            throw new ConnectionNotSetException("Соединение с БД не установлено, укажите корректные параметры соединения");
        } else if (selectRequest.equals("")) {
            throw new RequestIsEmptyException("Указан пустой запрос к БД");
        } else {
            return "Результат Select * from myTable";
        }
    }



}
