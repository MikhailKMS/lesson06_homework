package ru.sber.jd.exceptions;

import java.io.IOException;

public class ConnectionNotSetException extends IOException {

    public  ConnectionNotSetException(String message) {
        super(message);
    }

}
