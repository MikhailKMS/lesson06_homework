package ru.sber.jd.exceptions;

import java.sql.SQLException;

public class RequestIsEmptyException extends SQLException {

    public  RequestIsEmptyException(String message) {
        super(message);
    }

}
