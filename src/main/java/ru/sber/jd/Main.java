package ru.sber.jd;

import ru.sber.jd.dto.MSSQLServer;
import ru.sber.jd.dto.MyBD;
import ru.sber.jd.exceptions.ConnectionNotSetException;
import ru.sber.jd.exceptions.RequestIsEmptyException;

public class Main {


    public static void main(String[] args) {

        String resultRequest;

        MSSQLServer mySQLServerBD = new MSSQLServer();

        try {
            resultRequest = mySQLServerBD.readFromBD(new MyBD("MSSQL", ""), "");
        } catch (ConnectionNotSetException connex) {
            System.out.println(connex.getMessage());
        } catch (RequestIsEmptyException reqex) {
            System.out.println(reqex.getMessage());

        }

    }



}
